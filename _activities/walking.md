---
layout: activity
title:  Walking
categories: summer winter
feature-image: /activities/media/walking.jpg
feature-image-attribution: Department of Conservation, https://flic.kr/p/aSe9HF, CC BY 2.0
---
Walking is a great activity at St. Arnaud.
