---
layout: activity
title:  Kayaking
subtitle: Kayaking is a great activity at St. Arnaud.
categories: summer
feature-image: /activities/media/kayaking.jpg
---
Relaxing, scenic.
