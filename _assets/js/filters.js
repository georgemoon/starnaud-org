$(function() {
  'use strict';
  /* Filters objects (activities/places) based on selected filters */
  /* TODO: add URL # parameter to enable bookmarking/sharing of filters */

  // Initial state
  $('.filters .filter#all').parent().addClass('active');

  // Filters based on (a) all or (b) the id of the filter button
  $('.filters .filter').each(function(index) {
    var filter = $(this).attr('id');
    var filterSelector = '';
    var filterSelectorInverse = '';

    if(filter == 'all') {
      filterSelector = '.object';
      filterSelectorInverse = '';
    }
    else {
      filterSelector = '.object.' + filter;
      filterSelectorInverse = '.object:not(.' + filter + ')';
    }

    $(this).click(function() {
      /* Filter button state */
      $('.filters .filter').parent().removeClass('active');
      $(this).parent().addClass('active');

      /* Filtered objects state */
      $(filterSelector).parent().show();
      $(filterSelectorInverse).parent().hide();
    });
  });
});
