$(function() {
  /* Adds tooltips to photo attribution icon */
  $('[data-toggle="tooltip"]').tooltip();
});
