# starnaud.org

Jekyll static site for the community of St. Arnaud, New Zealand.

TODO: write contribution guidelines


## Getting Started
### Prerequisites
* ruby
* gem
* bundler
* bower
* ImageMagick

### Installation
1. `bundle install`
2. `bower install`

### Running
`jekyll serve`


## Troubleshooting
### I'm getting an error about assets
*(e.g. NilClass or logical path error)*
Try running `jekyll clean` then `jekyll serve`

### My images all look the same size
The Ruby tool `mini_magick` requires that ImageMagick be installed, make sure it
it installed and running correctly.


## License
starnaud.org website
Copyright (C) 2016 George Moon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
