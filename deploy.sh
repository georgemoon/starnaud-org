#!/bin/bash

export JEKYLL_ENV=production

jekyll clean
jekyll build

s3_website push

jekyll clean

export JEKYLL_ENV=development
