---
layout: page
title: About
permalink: /about/
no-nav: true
---

starnaud.org is maintained by [George Moon](http://moon.org.nz).

Source code
-----------
Built with open source technologies (Ruby, Jekyll, SCSS, Bootstrap, Bower).

This website's source code is hosted on [GitLab (georgemoon/starnaud-org)](https://gitlab.com/georgemoon/starnaud-org).
Pull requests are more than welcome.

Content re-use
--------------
<p>Copyright &copy; 2016 George Moon, except where otherwise noted.</p>
<p>Content is able to be re-used under the license terms below.</p>
<p><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">starnaud.org</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://moon.org.nz" property="cc:attributionName" rel="cc:attributionURL">George Moon</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.</p>
<p><a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a></p>
