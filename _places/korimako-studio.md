---
layout: place
title: Korimako Studio
categories: retail experience
location:
address:
phone: (03) 521 1999
web: http://janthomson.co.nz
email: janthomsonart@gmail.com
---
Jan Thomson is a New Zealand landscape painter, working in both watercolours and oils. She loves to get outside and paint, capturing light and mood and depicting places and everyday things people often take for granted. Her work is held in private collections in both New Zealand and overseas. View her paintings, calendars and cards at her local studio.
