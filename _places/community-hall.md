---
layout: place
title:  Community Hall
categories: community
location: -41.80054 172.85139
address: 22 Main Road, St. Arnaud 7072
phone: 021 041 7704
web: http://www.tasman.govt.nz/recreation/sportsgrounds-halls-recreation-centres/community-halls/lake-rotoiti-hall-st-arnaud/
email:
---
Community Hall is a place to do things.
