---
layout: place
title: Nelson Lakes Shuttles
categories: transport
location:
address: 5 Jenkins Place, Nelson 7011
phone: (03) 547 6896
web: http://nelsonlakesshuttles.co.nz
email: info@nelsonlakesshuttles.co.nz
---
<h2>Transport For Trampers</h2>
<p>New Zealand's shuttle transport for hikers, trekkers and trampers in Nelson Lakes National Park. Nelson Lakes Shuttles Service specializes in providing transport for trampers, hikers, climbers, mountain bikers including skiers. Information and assistance where possible on DOC hut tickets and gas canisters are part of our service. These can be ordered to meet your backcountry demands. Centrally based in St Arnaud we can service all tracks in the Nelson, Marlborough including West Coast South Island.</p>

<p>Nelson Lakes Shuttles provides a Shuttle Service in Nelson Lakes National Park including other areas on request. We can often arrive and depart when you require. No hassle charters for small or large groups. Please contact us for an obligation free quote. Our vans are fully seat belted, modern comfortable shuttles. Offering same day service with no bus changes.</p>
