---
layout: place
title:  Visitor Centre (DOC)
categories: community information
location:
address: 3 View Road, St. Arnaud 7072
phone: (03) 521 1806
web: http://www.doc.govt.nz/footer-links/contact-us/office-by-name/?office=K1024
email: nelsonlakesvc@doc.govt.nz
feature-image: /places/media/visitor-centre.jpg
feature-image-attribution: Department of Conservation, https://flic.kr/p/aSe9HF, CC BY 2.0
---
Kerr Bay Camp is a place to stay.
