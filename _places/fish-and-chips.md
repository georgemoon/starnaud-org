---
layout: place
title:  Fish & Chips
categories: food
location: -41.80157 172.84692
address: 74 Main Road, St. Arnaud 7072
phone: (03) 521 1086
web:
email:
---
<p>Fish & Chips are available in St. Arnaud on Fridays (5pm -> 7:30pm) and Saturdays (5pm -> 7:00pm)</p>
