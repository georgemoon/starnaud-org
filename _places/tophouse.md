---
layout: place
title: Tophouse Historic Inn
categories: accommodation food
location:
address: 68 Tophouse Road, St. Arnaud 7072
phone: (03) 521 1848
web: http://tophouse.kiwi/
email:
---
Tophouse is a place to stay and eat.
