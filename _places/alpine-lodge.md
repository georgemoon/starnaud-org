---
layout: place
title:  Alpine Lodge
categories: food accommodation
location: -41.80179 172.84776
address: 75 Main Road, St. Arnaud 7072
phone: (03) 521 1869
web: http://alpinelodge.co.nz
email: info@alpinelodge.co.nz
feature-image: /places/media/alpine-lodge.jpg
feature-image-attribution: John Abel, https://flic.kr/p/DBazE, CC BY-NC-ND 2.0
---
Alpine Lodge is a place to eat and stay.
