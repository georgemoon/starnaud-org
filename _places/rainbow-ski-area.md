---
layout: place
title: Rainbow Ski Area
categories: experience
location:
address: Rainbow Valley Road, St. Arnaud 7072
phone: (03) 521 1861
web: skirainbow.co.nz
email: info@skirainbow.co.nz
---
Rainbow Ski Area is cool.
