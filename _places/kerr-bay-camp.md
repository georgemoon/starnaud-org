---
layout: place
title:  Kerr Bay Camp (DOC)
categories: accommodation
location: -41.80577 -41.80577
address: 52 Lake Road, St. Arnaud 7072
phone: (03) 521 1808
web: http://www.doc.govt.nz/kerrbaycampsite
email: nelsonlakesvc@doc.govt.nz
feature-image: /places/media/kerr-bay-camp.jpg
feature-image-attribution: Department of Conservation, https://flic.kr/p/aSe9HF, CC BY 2.0
---
Kerr Bay Camp is a place to stay.
