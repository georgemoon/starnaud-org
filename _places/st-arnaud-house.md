---
layout: place
title:  St. Arnaud House
categories: accommodation
location:
address:
phone:
web: http://st-arnaudhouse.co.nz
email:
feature-image: /places/media/st-arnaud-house.jpg
feature-image-attribution: Russell Chilton, https://flic.kr/p/jo1xdP, CC BY 2.0
---
St. Arnaud House is a place to stay.
