module Jekyll
  module CategoryIconify
    def category_iconify(categories)
      categories_iconified = ""
      icon_type = ""
      categories.each do |category|
        case category
          when "food"
            icon_type = "fa-cutlery"
          when "accommodation"
            icon_type = "fa-bed"
          when "community"
            icon_type = "fa-users"
          when "retail"
            icon_type = "fa-shopping-bag"
          when "transport"
            icon_type = "fa-car"
          when "information"
            icon_type = "fa-info"
          when "experience"
            icon_type = "fa-ticket"
          when "summer"
            icon_type = "fa-sun-o"
          when "winter"
            icon_type = ""
          else
            icon_type = ""
        end
        output  = "<span class=\"icon\">"
        output += "<span class=\"fa-stack\">"
        output += "<i class=\"fa fa-square fa-stack-2x\"></i>"
        output += "<i class=\"fa fa-stack-1x fa-inverse #{icon_type}\" aria-hidden=\"true\"></i>"
        output += "</span>"
        output += "<span class=\"sr-only\">category #{category}</span>"
        output += "</span>"
        categories_iconified += output
      end
      return categories_iconified
    end
  end
end

Liquid::Template.register_filter(Jekyll::CategoryIconify)
