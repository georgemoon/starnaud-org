module Jekyll
  module CategoryClassify
    def category_classify(categories)
      categories_prepended = categories.map { |category| "category-" + category }
      return categories_prepended.join(" ")
    end
  end
end

Liquid::Template.register_filter(Jekyll::CategoryClassify)
