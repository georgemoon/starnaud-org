---
layout: page
title: Privacy
permalink: /privacy/
no-nav: true
---
This privacy policy is short and sweet. Basically (1) the site itself does not store user data and (2) Google Analytics is used for site use analysis.

**Last updated: 14 June 2016**

## Main
This site does not have any user data storage facility and hence does not track any data except for site analytics as noted below.

## Analytics
Like most websites, our website uses automatic data collection technology (Google Analytics) when you visit our website or use the services. We may collect information such as your IP address, internet service provider, browser type and language, referring and exit pages and URLs, date and time, amount of time spent on particular pages, what sections of the website you visit, number of links you click while on the website, search terms, operating system, website traffic and related statistics, keywords and/or other data. The privacy policy of Google Analytics is available at <https://www.google.co.nz/analytics/learn/privacy.html>
